# Modèle Scenari Lettre Actualités

## Description
Modèle Scenari qui permet d'éditer une lettre d'actualité.
Deux items racine :
- "lettre d'actualité COURRIEL" qui propose deux générateurs :
    - "lettre d'actualité version en ligne" à mettre en ligne sur internet, et qui contient les images de la lettre d'actualité
    - "lettre d'actualité encoyée par courriel : html à copier dans le mailer pour envoyer la lettre d'actualité (ne contient pas d'images, mais des urls vers les images de la version en ligne)
- "lettre d'actualité SITE" qui permet de rééditorialiser les articles dans des rubriques différentes


Depuis la lettre d'actualité envoyée par mail, un clic sur un lien permet d'ouvrir cette même lettre d'actualité sans sa version en ligne.
Depuis la lettre d'actualité envoyée par mail, un clic sur les rubriques permet d'ouvrir le site des lettre d'actualités.

## Compatibilité avec Opale
Les ressources et les blocs information/complément/fondamental sont compatibles avec le modèle Scenari Opale. Vous pouvez copier un contenu de Opale vers LettreActu et inversement.

## Comment utiliser les sources
Les présentes sources sont à utiliser dans ScenariBuilder 6.

## Modèle compilé
Le modèle Lettre d'actualité compilé est téléchargeable sur https://scenari.org/co/contributions.html

## Support
Vous pouvez me contacter par mail loic.alejandro [arobase] odigi.eu

## Auteurs
Ce modèle a été créé grace au financement de l'Université de Lille sous le pilotage de Kathy Casalino.

## License
Le code source de ce modèle applications est mis à disposition sous les quatre licences libres listées ci-dessous. Vous devez accepter au moins une de ces quatre licences pour exploiter tout ou partie du code source.
- MPL 1.1
- GPL 2.0
- LGPL 2.1
- CeCILL 2.0
