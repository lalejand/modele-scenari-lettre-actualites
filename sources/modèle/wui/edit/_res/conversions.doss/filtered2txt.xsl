<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
								xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
								xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
								xmlns:op="utc.fr:ics/opale3"
								version="1.0">
	<xsl:output method="xml" encoding="UTF-8"/>

	<xsl:template match="sp:filtered">
		<xsl:choose>
			<xsl:when test="//op:res/sp:txt">
				<xsl:apply-templates select="//op:res/sp:txt"/>
			</xsl:when>
			<xsl:when test="//op:res/sp:bkquote">
				<sp:txt>
					<op:txt>
						<xsl:apply-templates select="//op:res/sp:bkquote/*/*"/>
					</op:txt>
				</sp:txt>
			</xsl:when>
			<xsl:when test="//op:res/sp:txtRes">
				<xsl:apply-templates select="//op:res/sp:txtRes/op:txtRes/sp:txt"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- ### -->
	<!-- # Cas général : on copie -->
	<!-- # -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>