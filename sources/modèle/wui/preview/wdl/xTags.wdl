<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:anyCompositionPrim/>
	<sm:editPoints>
		<sm:tag refCodes="*">
			<sm:skippedWidget/>
		</sm:tag>
		<sm:tag refCodes="div">
			<sm:skippedWidget>
				<sm:container style="margin: 0.8rem 0 1rem 1rem;"/>
			</sm:skippedWidget>
		</sm:tag>
		<sm:tag refCodes="pb">
			<sm:skippedWidget>
				<sm:container style="margin: 0.5rem 0 0.5rem 1rem;"/>
			</sm:skippedWidget>
		</sm:tag>
		<sm:tag refCodes="intro piedPage rubrique article">
			<sm:openEdtWidget>
				<sm:headBodyContentBox collapsed="forbidden">
					<sm:cssRules xml:space="preserve">:host&gt;.head {
    font-size: 1.4rem;
    margin-bottom: 0.5rem;
    background-image: linear-gradient(to right, #434e52 0%,rgba(0,0,0,0) 100%);
    background-size: 100% 2px;
    background-position: bottom;
    background-repeat: no-repeat;
    padding-bottom: 4px;
}</sm:cssRules>
					<sm:head>
						<sm:extractFromModelCtx>
							<sm:name/>
						</sm:extractFromModelCtx>
					</sm:head>
					<sm:body>
						<sm:call>
							<sm:subModel/>
						</sm:call>
					</sm:body>
				</sm:headBodyContentBox>
			</sm:openEdtWidget>
		</sm:tag>
		<sm:tag refCodes="info comp basic">
			<sm:openEdtWidget>
				<sm:headBodyContentBox collapsed="forbidden">
					<sm:cssRules sc:refUri="/modèle/wui/preview/wdl/xTags.doss" xml:space="preserve">:host {
	background-image: linear-gradient(0deg, #434e52 0%, rgba(0, 0, 0, 0) 100%);
	background-position: right bottom;
	background-repeat: no-repeat;
	background-size: 1px 100%;
}

:host&gt;.body {
	background-color:rgba(224, 236, 236,0.5);
	border-radius:0 0 0 5px;
	background-image: linear-gradient(to right, #434e52 0%, rgba(0, 0, 0, 0) 100%);
	background-position: center top;
	background-repeat: no-repeat;
	background-size: 100% 1px;
	padding: 1px 4px 4px 4px;
	margin-bottom:15px;
	margin-inline-end:1px;
}
:host&gt;.head {
	font-size: 1.2em;
	margin-bottom: 0px;
	line-height: 2.5em;
	color:#434e52;
	text-align:end;
}
:host&gt;.head i.type {
	background:url("[CSSLIB]/blocks.svg") no-repeat scroll transparent;
	flex: 0 0 auto;
    height: 2.5em;
    margin-inline-end: 0.8rem;
    order: 2;
    padding-inline-start: 2.5em;
font-weight: bold;
}
:host&gt;.head span.title {
    flex: 1 1 auto;
    margin-inline-start: 0.8rem;
    order: 1;
    text-align: start;
}
:host([wed-name='info#'])&gt;.head i.type {
display: none;
}
:host([wed-name='info#']),
:host([wed-name='info#'])&gt;.body {
background: none;
}

:host([wed-name='basic#']){
	background-image: linear-gradient(0deg, #9F590E 0%, rgba(0, 0, 0, 0) 100%);
}
:host([wed-name='basic#'])&gt;.head i.type{
	color:#9F590E;
}
:host([wed-name='basic#'])&gt;.body{
	background-color:rgba(159, 89, 14, 0.2);
	background-image: linear-gradient(to right, #9F590E 0%, rgba(0, 0, 0, 0) 100%);
}

:host([wed-name='comp#'])&gt;.head i.type {
	background-position:0 -241px;
}
:host([wed-name='basic#'])&gt;.head i.type {
	background-position:0 -278px;
}</sm:cssRules>
					<sm:head>
						<i class="type">
							<span>
								<sm:extractFromModelCtx>
									<sm:name/>
								</sm:extractFromModelCtx>
							</span>
						</i>
						<span class="title">
							<sm:call>
								<sm:meta/>
							</sm:call>
						</span>
					</sm:head>
					<sm:body>
						<sm:call>
							<sm:subModel/>
						</sm:call>
					</sm:body>
				</sm:headBodyContentBox>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>