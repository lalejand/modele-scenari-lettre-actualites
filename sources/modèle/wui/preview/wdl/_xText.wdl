<?xml version="1.0"?>
<sm:textWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:anyTextPrim/>
	<sm:editPoints>
		<sm:root placeholder="￼;Écrivez votre texte￼">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="none !important"/>
				<sm:entryStyle key="width" value="100% !important"/>
			</sm:htmlStyle>
			<sm:freeHtmlStyle xml:space="preserve">
txt-root {
    padding: 0;
}

txt-li&gt;txt-para {
    margin-top: 0;
    margin-bottom: 0;
}</sm:freeHtmlStyle>
		</sm:root>
		<sm:blockTag tag="para">
			<sm:htmlStyle>
				<sm:entryStyle key="margin-top" value="4px"/>
				<sm:entryStyle key="margin-bottom" value="8px"/>
				<sm:entryStyle key="line-height" value="1.45em"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="itemizedList">
			<sm:behaviors>
				<sm:keyBinding char="*"/>
			</sm:behaviors>
			<sm:subBlockTag tag="listItem">
				<sm:htmlStyle>
					<sm:entryStyle key="margin-inline-start" value="1.2em"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:htmlStyle>
				<sm:entryStyle key="margin-inline-start" value="15px"/>
				<sm:entryStyle key="list-style" value="disc"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="orderedList">
			<sm:behaviors>
				<sm:keyBinding char="1"/>
			</sm:behaviors>
			<sm:subBlockTag tag="listItem">
				<sm:htmlStyle>
					<sm:entryStyle key="margin-inline-start" value="1.2em"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:htmlStyle>
				<sm:entryStyle key="margin-inline-start" value="12px"/>
				<sm:entryStyle key="list-style" value="decimal"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="table">
			<sm:subBlockTag tag="cell">
				<sm:htmlStyle>
					<sm:entryStyle key="border" value="1px solid var(--edit-color)"/>
					<sm:entryStyle key="color" value="var(--edit-color)"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="cell" role="word">
				<sm:htmlStyle>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="cell" role="num">
				<sm:htmlStyle>
					<sm:entryStyle key="text-align" value="end"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="row" role="head">
				<sm:htmlStyle>
					<sm:entryStyle key="font-weight" value="bold"/>
					<sm:entryStyle key="background-color" value="var(--inv-alt1-bgcolor)"/>
					<sm:entryStyle key="color" value="var(--inv-alt2-color)"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="column" role="head">
				<sm:htmlStyle>
					<sm:entryStyle key="font-weight" value="bold"/>
					<sm:entryStyle key="background-color" value="var(--inv-alt1-bgcolor)"/>
					<sm:entryStyle key="color" value="var(--inv-alt2-color)"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="caption">
				<sm:htmlStyle>
					<sm:entryStyle key="color" value="gray"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
		</sm:blockTag>
		<sm:inlineTag tag="uLink" role="acr">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="glos">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="bib">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="ref">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="docLnk">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt2-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="resLnk">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt2-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="ucLnk">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt2-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="phrase" role="url">
			<sm:behaviors>
				<sm:keyBinding char="U"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<!--<sm:entryStyle xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" key="text-decoration" value="underline"/>-->
				<sm:entryStyle key="direction" value="ltr"/>
				<sm:entryStyle key="text-decoration" value="underline"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="url"/>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="quote">
			<sm:behaviors>
				<sm:keyBinding char="I"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-family" value="&quot;Times New Roman&quot;, Times, serif"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="before">
				<sm:entryStyle key="font-weight" value="normal"/>
				<sm:entryStyle key="content" value="&quot;\&quot;&quot;"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="font-weight" value="normal"/>
				<sm:entryStyle key="content" value="&quot;\&quot;&quot;"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="emp">
			<sm:behaviors>
				<sm:keyBinding char="E"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-weight" value="bold"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="phrase" role="spec">
			<sm:behaviors>
				<sm:keyBinding char="T"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-style" value="italic"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="lang">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="code"/>
					</sm:dynamicString>
				</sm:addAttribute>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="code" prefix="￼;Langue : ￼" defaultValue="￼;Langue non spécifiée￼"/>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="code">
			<sm:htmlStyle>
				<sm:entryStyle key="font-family" value="var(--font-mono), monospace"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="gap">
			<sm:behaviors>
				<sm:keyBinding char="G"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px solid var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="exp">
			<sm:behaviors>
				<sm:keyBinding char="P"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-size" value="70%"/>
				<sm:entryStyle key="vertical-align" value="4px"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="ind">
			<sm:behaviors>
				<sm:keyBinding char="M"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-size" value="70%"/>
				<sm:entryStyle key="vertical-align" value="-4px"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="mathtex">
			<sm:behaviors>
				<sm:keyBinding char="K"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="direction" value="ltr"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="before">
				<sm:entryStyle key="content" value="'$'"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="content" value="'$'"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineImg" role="form">
			<sm:htmlStyle>
				<sm:entryStyle key="display" value="inline-block"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineImg" role="ico">
			<sm:htmlStyle>
				<sm:entryStyle key="display" value="inline-block"/>
			</sm:htmlStyle>
		</sm:inlineTag>
	</sm:editPoints>
</sm:textWdl>