<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/modèle/_lettreActuCourriel.model"/>
	<sm:model sc:refUri="/modèle/_lettreActuSiteWeb.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">.head{
color: var(--inv-alt1-color) !important;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:root>
		<sm:tag refCodes="rubrique piedPage">
			<sm:headBodyWidget layout="vertical">
				<sm:container>
					<sm:cssRules xml:space="preserve">.head{
    margin: 1.5em .1em .5em .1em;
    padding: .2em .7em;
    background-color: var(--inv-alt2-bgcolor);
    color: var(--inv-alt2-color);
    font-weight: bold;
    border-top-left-radius: .7em;
    border-top-right-radius: .7em;
    border-radius: .7em;
    align-self: flex-start;
}
.label {
min-width: initial;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>