<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/modèle/pb.model"/>
	<sm:editPoints>
		<sm:tag refCodes="info">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc"/>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="comp">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/modèle/wui/wdl/_tags/complement.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="basic">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/modèle/wui/wdl/_tags/basic.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>