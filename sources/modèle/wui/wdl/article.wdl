<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/modèle/article.model"/>
	<sm:model sc:refUri="/modèle/partie.model"/>
	<sm:editPoints>
		<sm:tag refCodes="pb">
			<sm:skippedWidget/>
		</sm:tag>
		<sm:tag refCodes="div">
			<sm:headBodyWidget layout="float">
				<sm:container class="bloc">
					<sm:cssRules xml:space="preserve">:host([wed-name='div#']){
    border-inline-start: 2px dotted var(--alt2-color);
}</sm:cssRules>
				</sm:container>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>