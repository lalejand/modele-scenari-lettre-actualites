<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/modèle/res/urlM.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:tag refCodes="title">
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">:host([wed-name='title#']) &gt; .body {
font-size:initial;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="doc">
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>