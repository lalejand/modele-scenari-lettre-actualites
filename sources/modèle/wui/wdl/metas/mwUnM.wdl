<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml" xmlns:wed="scenari.eu:wed">
	<sm:model sc:refUri="/modèle/res/mediaWeb/mwUnM.model"/>
	<sm:editPoints>
		<sm:tag refCodes="frag">
			<sm:openEdtWidget>
				<sm:content>
					<box-static class="v">
						<wed:children>
							<wed:display nodeType="text" wedlet="Box">
								<box-web-frag/>
							</wed:display>
						</wed:children>
					</box-static>
				</sm:content>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>