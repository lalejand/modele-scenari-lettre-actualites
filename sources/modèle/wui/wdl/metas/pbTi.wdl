<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/modèle/pbTi.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget>
				<sm:container style="display:contents"/>
			</sm:skippedWidget>
		</sm:root>
		<sm:tag refCodes="filters">
			<sm:skippedWidget/>
		</sm:tag>
		<sm:tag refCodes="title">
			<sm:headBodyWidget layout="horizontal-prefered">
				<sm:container style="margin-bottom:0.2em;">
					<sm:body style="font-size: 1.1em;"/>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>