<?xml version="1.0"?>
<sm:freeWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:wed="scenari.eu:wed" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/modèle/txtResM.model"/>
	<sm:wedlingLib libId="txtResM" sc:refUri="/modèle/wui/wdl/metas/txtResM.doss">
		<sm:jsFile fileName="txtResM.js" key="TXTRESM"/>
	</sm:wedlingLib>
	<sm:bindsRegistry>
		<wed:bind wedlet="Box" eltName="op:txtResM">
			<box-ctn class="v sm-content" skin="box/skipped" wed-name="op_txtResM#">
				<wed:slot>
					<wed:children/>
				</wed:slot>
			</box-ctn>
		</wed:bind>
		<wed:bind wedlet="Box" eltName="sp:pos" label="Position">
			<box-ctn class="h sm-property" skin="box/head-body" skinOver="box/head-body/block " hv="auto" wed-name="pos#">
				<div class="head">
					<box-label class="label">
						<sm:addAttribute name="title">
							<sm:extractFromModelCtx>
								<sm:quickHelp/>
							</sm:extractFromModelCtx>
						</sm:addAttribute>
					</box-label>
				</div>
				<div class="body">
					<wed:children>
						<wed:display nodeType="text" wedlet="Box">
							<op-box-txtres-pos-enum wed-name="pos#-input">
								<e k="lft" v="Texte à droite, Illustration à gauche"/>
								<e k="rgt" v="Texte à gauche, Illustration à droite"/>
							</op-box-txtres-pos-enum>
						</wed:display>
					</wed:children>
				</div>
			</box-ctn>
		</wed:bind>
	</sm:bindsRegistry>
</sm:freeWdl>