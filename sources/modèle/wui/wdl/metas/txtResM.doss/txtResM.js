export async function jslibAsyncInit(jsEndPoint) {
	const { DOM } = await jsEndPoint.importJs(":lib:commons/xml/dom.js");
	if (!customElements.get('op-box-txtres-pos-enum')) {
		class BoxTxtResPosEnum extends customElements.get("box-input-enum") {
			connectedCallback() {
				super.connectedCallback();
				if (this.binded)
					this.updateTxtRes(this.getKey());
			}
			refreshBindValue(val) {
				super.refreshBindValue(val);
				this.binded = true;
				if (this.isConnected)
					this.updateTxtRes(val);
			}
			updateTxtRes(pos) {
				if (!this.txtBox) {
					this.txtBox = DOM.findParent(this, null, (n) => {
						return DOM.IS_element(n) && n.getAttribute('wed-name') == 'op_txtRes#';
					});
				}
				if (this.txtBox) {
					if (pos)
						this.txtBox.setAttribute('imgPos', pos);
					else
						this.txtBox.removeAttribute('imgPos');
				}
			}
		}
		customElements.define('op-box-txtres-pos-enum', BoxTxtResPosEnum);
	}
}
