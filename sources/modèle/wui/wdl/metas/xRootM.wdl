<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/modèle/lettreActuCourrielM.model"/>
	<sm:editPoints>
		<sm:tag refCodes="urlNL urlBaseSite">
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">:host(.h) &gt; .head {
    flex: 1 1 16rem;
}
:host(.h) &gt; .body {
    flex: 8 2 20rem;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>