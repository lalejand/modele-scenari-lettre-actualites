<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/modèle/txtRes.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:widgetLib sc:refUri="/modèle/wui/wdl/metas/txtResM.doss">
					<sm:jsFile fileName="txtResM.js" key="TXTRESM"/>
				</sm:widgetLib>
				<sm:contentBox>
					<sm:cssRules xml:space="preserve">.grid {
	display: grid;
	grid-column-gap: 5px;
	grid-template-columns: 50% 50%;
	direction: rtl;
}

.grid &gt; slot {
	direction: ltr;
}

:host([imgPos=rgt]) &gt; .grid {
	direction: ltr;
}</sm:cssRules>
					<sm:call>
						<sm:meta/>
					</sm:call>
					<div class="grid">
						<sm:call>
							<sm:parts partsCodes="txt img" axisParts="#current"/>
						</sm:call>
					</div>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="txt">
			<sm:skippedWidget focusable="true"/>
		</sm:tag>
		<sm:tag refCodes="img">
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">::slotted(box-ptritem) {
    align-items:center;
}</sm:cssRules>
					<sm:head style="display:none;"/>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>