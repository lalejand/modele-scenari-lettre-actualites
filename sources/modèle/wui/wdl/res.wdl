<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/modèle/res.model"/>
	<sm:editPoints>
		<sm:tag refCodes="txt txtRes">
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules sc:refUri="/modèle/wui/edit/_res/icons.doss" xml:space="preserve">:host([wed-name='txt#']):before,
:host([wed-name='txtRes#']):before {
    background-repeat: no-repeat;
    content: " ";
    height: 20px;
    background-position: center center;
}

:host([wed-name='txt#'])&gt;.head,
:host([wed-name='txtRes#'])&gt;.head {
    display: none;
}

:host([wed-name='txt#']):before,
:host([wed-name='txtRes#']):before {
    background: left url("[CSSLIB]/txt.svg") no-repeat;
}

:host([wed-name='txt#']:lang(ar)):before,
:host([wed-name='txtRes#']:lang(ar)):before {
	transform: scaleX(-1);
}

</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<!--<sm:tag xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" refCodes="res int">
	<sm:headBodyWidget>
		<sm:container>
			<sm:cssRules xml:space="preserve">.body{
	flex-direction: row-reverse;
}

::slotted(*) {
margin: 0 5px;
flex: 1 1 10em;
}
::slotted(box-ptritem) {
    align-items:center;
}</sm:cssRules>
		</sm:container>
	</sm:headBodyWidget>
</sm:tag>-->
		<sm:tag refCodes="res int">
			<sm:openEdtWidget>
				<sm:contentBox>
					<sm:cssRules xml:space="preserve">.body{
	display: grid;
	grid-column-gap: 5px;
	grid-template-columns: 49% 49%;
}

::slotted(box-ptritem) {
    align-items:center;
margin:0;
}</sm:cssRules>
					<sm:container sharedCssKey="box/head-body box/head-body/block"/>
					<div class="head">
						<box-label/>
					</div>
					<div class="body">
						<sm:call>
							<sm:subModel/>
						</sm:call>
						<sm:call>
							<sm:meta/>
						</sm:call>
					</div>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:tag>
		<sm:tag refCodes="filtered">
			<sm:headBodyWidget layout="float"/>
		</sm:tag>
		<!--<sm:tag xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" refCodes="res int">
	<sm:openEdtWidget>
		<sm:contentBox sharedCssKey="box/head-body/float">
			<sm:extractFromModelCtx>
				<sm:name/>
			</sm:extractFromModelCtx>
			<sm:call>
				<sm:subModel/>
			</sm:call>
			<sm:call>
				<sm:meta/>
			</sm:call>
			<!-~-<box-collaps xmlns="http://www.w3.org/1999/xhtml" class="v labelStruct" skin="box/head-body" skinOver="box/head-body/block ">
	<div class="head">
		<box-label class="label">
			<sm:addAttribute xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" name="title">￼;Comportement de la ressource￼</sm:addAttribute>
		</box-label>
	</div>
	<div class="body">
		<sm:call xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
			<sm:meta/>
		</sm:call>
	</div>
</box-collaps>-~->
		</sm:contentBox>
	</sm:openEdtWidget>
</sm:tag>-->
	</sm:editPoints>
</sm:compositionBoxWdl>