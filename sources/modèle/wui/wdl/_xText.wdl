<?xml version="1.0"?>
<sm:textWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns="http://www.w3.org/1999/xhtml" xmlns:wed="scenari.eu:wed">
	<sm:anyTextPrim/>
	<sm:editPoints>
		<sm:root/>
		<sm:blockTag tag="para">
			<sm:htmlStyle>
				<sm:entryStyle key="margin-top" value="2px"/>
				<sm:entryStyle key="margin-bottom" value="4px"/>
				<sm:entryStyle key="line-height" value="1.25em"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="itemizedList">
			<sm:behaviors>
				<sm:keyBinding char="*"/>
			</sm:behaviors>
			<sm:subBlockTag tag="listItem">
				<sm:htmlStyle>
					<sm:entryStyle key="margin-inline-start" value="1.2em"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:htmlStyle>
				<sm:entryStyle key="margin-inline-start" value="15px"/>
				<sm:entryStyle key="list-style" value="disc"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="orderedList">
			<sm:behaviors>
				<sm:keyBinding char="1"/>
			</sm:behaviors>
			<sm:subBlockTag tag="listItem">
				<sm:htmlStyle>
					<sm:entryStyle key="margin-inline-start" value="1.2em"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:htmlStyle>
				<sm:entryStyle key="margin-inline-start" value="12px"/>
				<sm:entryStyle key="list-style" value="decimal"/>
			</sm:htmlStyle>
		</sm:blockTag>
		<sm:blockTag tag="table">
			<sm:subBlockTag tag="cell">
				<sm:htmlStyle>
					<sm:entryStyle key="border" value="1px solid var(--edit-color)"/>
					<sm:entryStyle key="color" value="var(--edit-color)"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="cell" role="word">
				<sm:htmlStyle>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="cell" role="num">
				<sm:htmlStyle>
					<sm:entryStyle key="text-align" value="end"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="row" role="head">
				<sm:htmlStyle>
					<sm:entryStyle key="font-weight" value="bold"/>
					<sm:entryStyle key="background-color" value="var(--inv-alt1-bgcolor)"/>
					<sm:entryStyle key="color" value="var(--inv-alt2-color)"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="column" role="head">
				<sm:htmlStyle>
					<sm:entryStyle key="font-weight" value="bold"/>
					<sm:entryStyle key="background-color" value="var(--inv-alt1-bgcolor)"/>
					<sm:entryStyle key="color" value="var(--inv-alt2-color)"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
			<sm:subBlockTag tag="caption">
				<sm:htmlStyle>
					<sm:entryStyle key="color" value="gray"/>
					<sm:entryStyle key="text-align" value="center"/>
				</sm:htmlStyle>
			</sm:subBlockTag>
		</sm:blockTag>
		<sm:inlineTag tag="uLink" role="url">
			<sm:htmlStyle>
				<sm:entryStyle key="direction" value="ltr"/>
				<sm:entryStyle key="text-decoration" value="underline"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="acr">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="glos">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="bib">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="ref">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt1-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="docLnk">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt2-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="resLnk">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt2-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="uLink" role="ucLnk">
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px dashed var(--alt2-border-color)"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="phrase" role="url">
			<sm:behaviors>
				<sm:keyBinding char="U"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<!--<sm:entryStyle xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" key="text-decoration" value="underline"/>-->
				<sm:entryStyle key="direction" value="ltr"/>
				<sm:entryStyle key="text-decoration" value="underline"/>
				<sm:entryStyle key="color" value="var(--alt2-color)"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="url"/>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="quote">
			<sm:behaviors>
				<sm:keyBinding char="I"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-family" value="&quot;Times New Roman&quot;, Times, serif"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="before">
				<sm:entryStyle key="font-weight" value="normal"/>
				<sm:entryStyle key="content" value="&quot;\&quot;&quot;"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="font-weight" value="normal"/>
				<sm:entryStyle key="content" value="&quot;\&quot;&quot;"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="emp">
			<sm:behaviors>
				<sm:keyBinding char="E"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-weight" value="bold"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="spec">
			<sm:behaviors>
				<sm:keyBinding char="P"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-style" value="italic"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="lang">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="code"/>
					</sm:dynamicString>
				</sm:addAttribute>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeMetaSelect querySelector="code" prefix="￼;Langue : ￼" defaultValue="￼;Langue non spécifiée￼"/>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="code">
			<sm:htmlStyle>
				<sm:entryStyle key="font-family" value="var(--font-mono), monospace"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="gap">
			<sm:behaviors>
				<sm:keyBinding char="&amp;"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="border" value="1px solid var(--inv-alt1-bgcolor)"/>
				<sm:entryStyle key="border-radius" value="0.2em"/>
				<sm:entryStyle key="color" value="var(--inv-alt1-color)"/>
				<sm:entryStyle key="background-color" value="var(--inv-alt1-bgcolor)"/>
			</sm:htmlStyle>
			<sm:handler>
				<sm:addAttribute name="title">
					<sm:dynamicString>
						<sm:freeJsValue xml:space="preserve">function getTitle(){
	let vRet = "";
	let vSyn = this.metas ? this.metas.querySelector("synonym") : null;
	let vOpt = this.metas ? this.metas.querySelector("option") : null;
	if(vSyn &amp;&amp; vSyn.textContent){
		vRet += "￼Synonymes :￼ ";
		vRet += vSyn.textContent;
		vSyn = vSyn.nextSibling;
		while (vSyn &amp;&amp; vSyn.textContent) {
			vRet += ", " + vSyn.textContent;
			vSyn = vSyn.nextSibling;
		}
		if(vOpt &amp;&amp; vOpt.textContent) vRet += " / ";
	}
	if(vOpt &amp;&amp; vOpt.textContent){
		vRet += "￼Options :￼ ";
		vRet += vOpt.textContent;
		vOpt = vOpt.nextSibling;
		while (vOpt &amp;&amp; vOpt.textContent) {
			vRet += ", " + vOpt.textContent;
			vOpt = vOpt.nextSibling;
		}
	}
	return vRet;
}</sm:freeJsValue>
					</sm:dynamicString>
				</sm:addAttribute>
			</sm:handler>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="exp">
			<sm:behaviors>
				<sm:keyBinding char="&lt;"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-size" value="70%"/>
				<sm:entryStyle key="vertical-align" value="4px"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="ind">
			<sm:behaviors>
				<sm:keyBinding char="_"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="font-size" value="70%"/>
				<sm:entryStyle key="vertical-align" value="-4px"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="textLeaf" role="mathtex">
			<sm:behaviors>
				<sm:keyBinding char="K"/>
			</sm:behaviors>
			<sm:htmlStyle>
				<sm:entryStyle key="direction" value="ltr"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="before">
				<sm:entryStyle key="content" value="'$'"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="content" value="'$'"/>
				<sm:entryStyle key="color" value="var(--alt1-color)"/>
			</sm:htmlStyle>
			<sm:previewBox>
				<sm:freeWedlet wedlet="Box">
					<sm:content>
						<box-static class="v">
							<wed:children select="* # !">
								<wed:bind nodeType="text" wedlet="Box">
									<box-transformer transform="transform=latex2img&amp;resolution=96&amp;content={0}" refreshLatencyInMs="1000" style="padding:3px;border-radius:3px;background-color:white;color:black;"/>
								</wed:bind>
							</wed:children>
						</box-static>
					</sm:content>
				</sm:freeWedlet>
			</sm:previewBox>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineImg" role="form">
			<sm:htmlStyle>
				<sm:entryStyle key="display" value="inline-block"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineImg" role="ico">
			<sm:htmlStyle>
				<sm:entryStyle key="display" value="inline-block"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="loc">
			<sm:htmlStyle>
				<sm:entryStyle key="font-weight" value="bold"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="content" value="' : '"/>
			</sm:htmlStyle>
		</sm:inlineTag>
		<sm:inlineTag tag="inlineStyle" role="desc">
			<sm:htmlStyle>
				<sm:entryStyle key="font-style" value="italic"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="before">
				<sm:entryStyle key="content" value="'['"/>
			</sm:htmlStyle>
			<sm:htmlStyle pseudoClass="after">
				<sm:entryStyle key="content" value="']'"/>
			</sm:htmlStyle>
		</sm:inlineTag>
	</sm:editPoints>
</sm:textWdl>