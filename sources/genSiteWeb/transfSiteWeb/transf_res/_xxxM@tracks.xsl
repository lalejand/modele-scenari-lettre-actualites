<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="sc sp">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:template match="*">
		<xsl:apply-templates select="sp:alt/sp:*"/>
	</xsl:template>
	<xsl:template match="sp:subTitles">
		<!-- Info si une vidéo était dans les sous titres -->
		<xsl:value-of select="if(typeAgent(concat('@', gotoSubModel(sp:subTitle))) = 'sfile_mp4_f4v_ogv_webm', warning(&quot;ATTENTION : l'utilisation d'une vidéo dans les sous-titres est dépréciée&quot;))"/>
		<!-- // -->
		<track kind="subtitles" src="{getUrl(gotoSubModel(sp:subTitle), 'url')}" srclang="{sp:lang}"/>
	</xsl:template>
	<xsl:template match="sp:audioDesc">
		<!-- Info si une vidéo était dans audiodesc -->
		<xsl:value-of select="if(typeAgent(concat('@', gotoSubModel())) = 'sfile_mp4_f4v_ogv_webm', warning(&quot;ATTENTION : l'utilisation d'une vidéo dans l'audiodescription est dépréciée&quot;))"/>
		<!-- // -->
		<audio-track kind="descriptions" src="{getUrl(gotoSubModel(), 'url')}"/>
	</xsl:template>
	<xsl:template match="sp:transcript">
		<html-track kind="captions">
			<xsl:value-of select="getContent(gotoSubModel())" disable-output-escaping="yes"/>
		</html-track>
	</xsl:template>
	<xsl:template match="sp:altVideo">
		<video-track kind="sign" src="{getUrl(gotoSubModel(), 'url')}"/>
	</xsl:template>
</xsl:stylesheet>