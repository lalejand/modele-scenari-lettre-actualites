1
00:00:01,896 --> 00:00:16,025
Comment les abeilles fabriquent-elles du miel ?

2
00:00:16,025 --> 00:00:19,576
L'histoire du miel commence
avec une histoire de sexe.

3
00:00:19,576 --> 00:00:21,372
Celle des fleurs.

4
00:00:21,372 --> 00:00:24,645
Ces fleurs se reproduisent
de façon sexuée, comme nous.

5
00:00:24,645 --> 00:00:27,054
Tout commence avec la rencontre du pollen

6
00:00:27,054 --> 00:00:29,432
qui joue le rôle du spermatozoïde chez la fleur

7
00:00:29,432 --> 00:00:32,055
et du pistil qui contient les ovules.

8
00:00:32,055 --> 00:00:34,213
Il faut donc que le pollen d'une plante

9
00:00:34,213 --> 00:00:36,957
entre en contact avec le pistil
d'une autre plante.

10
00:00:36,957 --> 00:00:39,417
Mais comme les plantes ne peuvent
pas se déplacer,

11
00:00:39,417 --> 00:00:42,086
il faut que quelqu'un
ou quelque chose s'en charge.

12
00:00:42,086 --> 00:00:44,277
Certaines plantes comptent sur le vent,

13
00:00:44,277 --> 00:00:46,280
mais ce n'est pas très efficace.

14
00:00:46,280 --> 00:00:48,639
Les pollens voyagent de façon aléatoire

15
00:00:48,639 --> 00:00:52,364
et ils ont plus de chance de tomber par terre
que sur un pistil.

16
00:00:52,364 --> 00:00:56,396
Alors, la plupart des fleurs ont opté
pour une autre solution.

17
00:00:56,396 --> 00:00:58,753
Elles s'arrangent pour que ce soit des insectes qui,

18
00:00:58,753 --> 00:01:00,510
sans s'en rendre compte,

19
00:01:00,510 --> 00:01:03,332
transportent des grains de pollen
d'une fleur à l'autre.

20
00:01:03,332 --> 00:01:05,887
Pour cela, elles rivalisent d'ingéniosité

21
00:01:05,887 --> 00:01:08,623
pour attirer les insectes dans leur fleur :

22
00:01:08,623 --> 00:01:12,439
couleurs vives des pétales,
odeurs parfumées et nourriture.

23
00:01:12,439 --> 00:01:15,632
Et quoi de plus alléchant que du sucre ?

24
00:01:15,632 --> 00:01:17,276
Tout au fond de la fleur,

25
00:01:17,276 --> 00:01:18,672
au pied des pétales,

26
00:01:18,672 --> 00:01:20,340
la plante secrète le nectar,

27
00:01:20,340 --> 00:01:22,468
de l'eau très sucrée.

28
00:01:22,468 --> 00:01:24,750
Les insectes,
comme par exemple le papillon,

29
00:01:24,750 --> 00:01:26,797
raffolent de ce nectar.

30
00:01:26,797 --> 00:01:29,246
Ils volent de fleur en fleur pour se nourrir.

31
00:01:29,246 --> 00:01:31,386
Au passage, des grains de pollen

32
00:01:31,386 --> 00:01:33,416
s'accrochent dans leurs pattes poilues

33
00:01:33,416 --> 00:01:35,443
et se décrochent quelques fleurs plus loin.

34
00:01:35,443 --> 00:01:37,061
Et le tour est joué !

35
00:01:37,061 --> 00:01:38,765
Non seulement le pollen voyage,

36
00:01:38,765 --> 00:01:41,044
mais en plus il a de fortes chances

37
00:01:41,044 --> 00:01:43,326
d'arriver sur le pistil d'une autre plante.

38
00:01:43,326 --> 00:01:45,638
Les abeilles font comme les papillons,

39
00:01:45,638 --> 00:01:48,330
sauf qu'elles n'avalent pas
complètement le nectar.

40
00:01:48,330 --> 00:01:50,343
Elles le stockent dans un estomac spécial

41
00:01:50,343 --> 00:01:51,968
qu'on appelle le jabot.

42
00:01:51,968 --> 00:01:55,562
Dans le jabot commence
la transformation chimique du nectar.

43
00:01:55,562 --> 00:01:57,387
Le sucre y est prédigéré

44
00:01:57,387 --> 00:01:58,959
comme dans notre estomac.

45
00:01:58,959 --> 00:02:01,667
Les sucres complexes comme le saccharose

46
00:02:01,667 --> 00:02:03,952
sont transformés en sucres plus simples :

47
00:02:03,952 --> 00:02:06,246
le glucose et le fructose,

48
00:02:06,246 --> 00:02:09,230
directement assimilables
par l'organisme dans le sang.

49
00:02:09,230 --> 00:02:12,359
Dans le jabot, le nectar est aussi asséché.

50
00:02:12,359 --> 00:02:15,712
Et donc sa concentration en sucre augmente.

51
00:02:15,712 --> 00:02:19,780
De retour à la ruche, l'abeille régurgite le nectar

52
00:02:19,780 --> 00:02:21,989
qui est ensuite ré-ingurgité par d'autres abeilles,

53
00:02:21,989 --> 00:02:24,248
afin de compléter le processus.

54
00:02:24,248 --> 00:02:27,163
Puis le nectar qui est déjà quasiment du miel,

55
00:02:27,163 --> 00:02:28,974
est versé dans des alvéoles de cires

56
00:02:28,974 --> 00:02:31,530
préparées par des abeilles bâtisseuses.

57
00:02:31,530 --> 00:02:35,327
Les abeilles ventileuses finissent
d'assécher le miel.

58
00:02:35,327 --> 00:02:38,630
Dès que la teneur en eau descend sous les 20%,

59
00:02:38,630 --> 00:02:40,578
les abeilles referment l'alvéole

60
00:02:40,578 --> 00:02:42,246
et le miel peut être conservé

61
00:02:42,246 --> 00:02:45,124
pour servir de nourriture pendant l'hiver suivant.

62
00:02:45,124 --> 00:02:47,047
Et même bien plus tard !

63
00:02:47,047 --> 00:02:50,431
On dit que le miel trouvé dans les tombes des pharaons Egyptiens

64
00:02:50,431 --> 00:02:52,542
était encore comestible.

65
00:02:52,542 --> 00:02:54,973
Le miel récolté par l'apiculteur

66
00:02:54,973 --> 00:02:57,472
a demandé beaucoup de travail aux abeilles.

67
00:02:57,472 --> 00:02:58,775
A chaque voyage,

68
00:02:58,775 --> 00:03:03,537
l'abeille peut ramener
jusqu'à 70 mg de nectar dans son jabot,

69
00:03:03,537 --> 00:03:07,267
qui donneront au final moins de 25 mg de miel.

70
00:03:07,267 --> 00:03:10,872
Pour cela,
l'abeille doit butiner environ 500 fleurs

71
00:03:10,872 --> 00:03:14,328
et parcourir parfois plusieurs kilomètres.

72
00:03:14,328 --> 00:03:17,235
Pour remplir un pot de 500g de miel,

73
00:03:17,235 --> 00:03:19,634
il faut donc environ 20 000 voyages,

74
00:03:19,634 --> 00:03:21,659
pratiquement le tour de la Terre,

75
00:03:21,659 --> 00:03:23,181
et 10 000 000 de fleurs,

76
00:03:23,181 --> 00:03:26,305
soit environ 7 000 heures de travail.
